/**
 * @author rsaraiva
 */
package bean;

import dao.GenericDAO;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import model.Event;

@Named
@RequestScoped
public class InjectionBean implements Serializable {
    
    @Inject
    private GenericDAO<Event> dao;
    
    @PostConstruct
    private void init() {
        System.out.println(dao);
    }

    public GenericDAO<Event> getDao() {
        return dao;
    }
}
