/**
 * @author rsaraiva
 */
package producer;

import java.lang.reflect.ParameterizedType;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import dao.GenericDAO;

public class DAOProducer {

    @Produces
    public <T extends Object> GenericDAO<T> createGenericDAO(InjectionPoint injectionPoint) {
        ParameterizedType type = (ParameterizedType) injectionPoint.getType();
        Class classe = (Class) type.getActualTypeArguments()[0];
        return new GenericDAO(classe);
    }
}
