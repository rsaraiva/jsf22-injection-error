/**
 * @author rsaraiva
 */
package dao;

import java.io.Serializable;

public class GenericDAO<T> implements Serializable {
    
    private Class<T> classe;

    public GenericDAO(Class<T> classe) {
        this.classe = classe;
    }
}
